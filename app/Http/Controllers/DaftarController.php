<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisTest;
use App\Jadwal;
use App\Peserta;
use App\Daftar;
use App\User;
use App\Bayar;
use DB;
use Auth;

class DaftarController extends Controller
{

    public function index()
    {
        $data_daftar = Daftar::join('peserta','peserta.id_peserta','=','daftar.id_peserta')
                                ->join('jadwal','jadwal.id_jadwal','=','daftar.id_jadwal') 
                                ->get(['daftar.id_daftar','peserta.nama','jadwal.jadwal_test','daftar.status','jadwal.id_test']);
        $data_peserta = Peserta::all();
        $data_jadwal = Jadwal::join('jenis_test','jenis_test.id_test','=','jadwal.id_test')
                                ->get(['jenis_test.id_test','jenis_test.nama_test']);
        $data_test = JenisTest::all();
        return view('daftar',compact('data_daftar','data_peserta','data_jadwal','data_test'));
    }

    public function create(Request $request)
    {
        \App\Daftar::create($request->all());
        return redirect('/daftar')->with('sukses','Data Berhasil Di input');
    }

    public function edit($id_daftar)
    {
        $daftar = \App\Daftar::find($id_peserta);
        return view('editstatus',['daftar' => $daftar]);
    }

    public function update(Request $request,$id_daftar)
    {
        $daftar = \App\Daftar::find($id_peserta);
        $daftar->update($request->all());
        return redirect ('/daftar')->with('sukses','Data Berhasil Di update');
    }

    public function delete($id_daftar)
    {
        $daftar_delete = \App\Daftar::find($id_daftar);
        $daftar_deletet->delete();
        return redirect('/daftar')->with('sukses','Data Berhasil Di hapus');
    }

    public function viewDaftar($id_test)
    {
        $a = Daftar::join('jadwal','jadwal.id_jadwal','=','daftar.id_jadwal')
                ->get(['daftar.id_daftar','jadwal.jadwal_test','daftar.status','jadwal.id_test']);

        $b = Jadwal::join('jenis_test','jenis_test.id_test','=','jadwal.id_test')
                ->get(['jenis_test.id_test','jenis_test.nama_test']);

        $data = Jadwal::where('id_test',$id_test)->get();
        return view('daftar',compact('a','b','data'));
    }

    public function pilihJadwal($id_jadwal, Request $request)
    {
        $daftar = new Daftar;
        $daftar->id=Auth::user()->id;
        $daftar->id_jadwal = $request->id_jadwal;
        $daftar->status=0; //waitinglist
        $daftar->save();
        return redirect('dashboard')->with('sukses','Data Berhasil Di input');;
    }

    
    
}

@extends('layouts.app')
@section('content')
<h1>BIODATA</h1>
<div class="container-fluid">
  <div class="row">
    <div class="col col-lg-4 col-md-4">
      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <!-- <img src="{{ asset('img/user1-128x128.jpg') }}" alt="profil" class="profile-user-img img-responsive img-circle"> -->
          </div>
          @foreach($data_peserta as $peserta)
          <h3 class="profile-username text-center">{{$peserta-> nama}}</h3>
          <hr>
          <strong>
            <i class="fas fa-map-marker mr-2"></i>
            {{$peserta-> jenis_kelamin}}
          </strong>
          <p class="text-muted">
          {{$peserta-> alamat}}
          </p>
          <hr>
          <strong>
            <i class="fas fa-envelope mr-2"></i>
            {{$peserta-> agama}}
          </strong>
          <p class="text-muted">
          {{$peserta-> email}}
          </p>
          <hr>
          <strong>
            <i class="fas fa-phone mr-2"></i>
            {{$peserta-> no_hp}}
          </strong>
          <hr>
          @endforeach
          <a href="#" class="btn btn-primary btn-block">Setting</a>
        </div>
      </div>      
    </div>
@endsection
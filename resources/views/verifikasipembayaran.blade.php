@extends('layouts.admin')

@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Verifikasi Pembayaran</h1>
            </div>
                <table class = "table table-striped">
                    <tr>
                        <th>No</th>
                        <th>Id Daftar</th>
                        <th>Nama</th>
                        <th>Tes</th>
                        <th>Jadwal</th>
                        <th>Harga</th>
                        <th>Status Test</th>
                        <th>Bukti Transaksi</th>
                        <th>Status Pembayaran</th>
                        <th>Aksi</th>
                        <th></th>
                        
                    </tr>
                    
                    @foreach($data_daftar as $item)
                    <tr>
                    @if($item -> status ==1)
                        <td>{{$loop-> iteration}}</td>
                        <td>{{$item -> id_daftar}}</td>
                        <td>{{DB::table('users')->where('id', $item['id'])->value('name')}}</td>
                        <td>{{DB::table('jenis_test')->where('id_test',DB::table('jadwal')->where('id_jadwal', $item['id_jadwal'])->value('id_test'))->value('nama_test')}}</td>
                        <td>{{DB::table('jadwal')->where('id_jadwal', $item['id_jadwal'])->value('jadwal_test')}}</td>
                        <td>{{DB::table('jenis_test')->where('id_test',DB::table('jadwal')->where('id_jadwal', $item['id_jadwal'])->value('id_test'))->value('biaya')}}</td>
                        
                    
                        @if($item->status == 0)
                                        <td>Menunggu</td>
                                        @elseif($item->status == 1)
                                        <td>Diterima</td>
                                        <td><a href="storage/images/{{$item->bukti}}">Tekan Disini</a></td>
                                                @if($item->status_pembayaran == 0)
                                                <td>Menunggu</td>
                                                <td>
                                                <form method="post" action="/verifikasipembayaran/pilih">
                                                @csrf
                                                    <input type="hidden" name="id_daftar" value="{{$item->id_daftar}}">
                                                    <input type="hidden" name="status_pembayaran" value="1">
                                                    <button type="submit" class="btn btn-success"> Terima </button>
                                                </form>
                                                </td>
                                                <td><form method="post" action="/verifikasipembayaran/pilih">
                                                @csrf
                                                            <input type="hidden" name="status_pembayaran" value="2">
                                                            <input type="hidden" name="id_daftar" value="{{$item->id_daftar}}">
                                                            <button type="submit" class="btn btn-danger"> Tolak </button>
                                                            </form>
                                                </td>
                                                @elseif($item->status_pembayaran == 1)
                                                <td>Diterima</td>
                                                <td><form method="post" action="/verifikasipembayaran/pilih">
                                                @csrf
                                                            <input type="hidden" name="status_pembayaran" value="2">
                                                            <input type="hidden" name="id_daftar" value="{{$item->id_daftar}}">
                                                            <button type="submit" class="btn btn-danger"> Tolak </button>
                                                            </form>
                                                </td>
                                                
                                                @else
                                                <td>Ditolak</td>
                                                <td>
                                                <form method="post" action="/verifikasipembayaran/pilih">
                                                @csrf
                                                    <input type="hidden" name="id_daftar" value="{{$item->id_daftar}}">
                                                    <input type="hidden" name="status_pembayaran" value="1">
                                                    <button type="submit" class="btn btn-success"> Terima </button>
                                                </form>
                                                </td>
                                                @endif
                                        @else
                                        <td>Ditolak</td>
                        @endif
                    @endif
                        
                    </tr>
                    @endforeach
                </table>
        </div>
    </div>    
    </div>         
@endsection
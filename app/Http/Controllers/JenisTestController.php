<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JenisTestController extends Controller
{
    public function index()
    {
        $data_jenistest = \App\JenisTest::all();
        return view('jenistest',['data_jenistest' => $data_jenistest]);
    }
    

    public function create(Request $request)
    {
        \App\JenisTest::create($request->all());
        return redirect('/jenistest')->with('sukses','Data Berhasil Di input');
    }
    public function delete($id_test)
    {
        $jenis_test = \App\JenisTest::find($id_test);
        $jenis_test->delete();
        return redirect('/jenistest')->with('sukses','Data Berhasil Di hapus');
    }
}

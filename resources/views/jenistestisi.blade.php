@extends('layouts.admin')
@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <h1>Input jenis test</h1>
    <div>
                        <form action = "/jenistest/create" method="POST">
                        {{csrf_field()}}
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nama</label>
                                <input name="nama_test" type="text" class="form-control" id="nama_test" aria-describedby="emailHelp" placeholder="Nama Test">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Harga</label>
                                <input name="biaya" type="text" class="form-control" id="nama_test" aria-describedby="emailHelp" placeholder="Harga">
                            </div>
                            <div class="mb-3">
                            <label for="exampleInputEmail1">Keterangan</label>
                                <div class="form-floating">
                                    <textarea name="keterangan" class="form-control" placeholder="Keterangan" id="floatingTextarea2" style="height: 100px"></textarea>
                                    <label for="floatingTextarea2">Keterangan</label>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
    
    
@endsection
@section('footer')
@endsection
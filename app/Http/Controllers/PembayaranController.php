<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisTest;
use App\Daftar;
use App\Bayar;
use App\User;
use App\DB;
use Auth;

class PembayaranController extends Controller
{
    public function upload($id_daftar){
		$auth = Auth::user()->id;
        $data_daf= Daftar::where([
            ['id', '=', $auth],
            ])->value('id_daftar');
        //$data_index = tes::all();
        return view('pembayaran',compact('data_daf','id_daftar'));
	}
 
	public function proses_upload(Request $request){

        $request->validate([
            'bukti' => 'required',
            'bukti.*' => 'mimes:doc,docx,PDF,pdf,jpg,jpeg,png|max:2000'
        ]);
        if ($request->hasfile('bukti')) {            
            $filename = round(microtime(true) * 1000).'-'.str_replace(' ','-',$request->file('bukti')->getClientOriginalName());
            $request->file('bukti')->storeAs('public/images',$filename);
             Daftar::where('id_daftar',$request->get('id_daftar'))->update([
                 'bukti' => $filename
             ]);
            return redirect('/statuspembayaran')->with('sukses','Pembayaran berhasil! Mohon menunggu konfirmasi dari Admin');
        }
       
		// $this->validate($request, [
		// 	'bukti' => 'required',
		// ]);
 
		// // menyimpan data file yang diupload ke variabel $file
		// $bukti = $request->file('bukti');

 
      	//         // nama file
		// echo 'File Name: '.$file->getClientOriginalName();
		// echo '<br>';
 
      	//         // ekstensi file
		// echo 'File Extension: '.$file->getClientOriginalExtension();
		// echo '<br>';
 
      	//         // real path
		// echo 'File Real Path: '.$file->getRealPath();
		// echo '<br>';
 
      	//         // ukuran file
		// echo 'File Size: '.$file->getSize();
		// echo '<br>';
 
      	//         // tipe mime
		// echo 'File Mime Type: '.$file->getMimeType();
 
      	//         // isi dengan nama folder tempat kemana file diupload
		// $tujuan_upload = 'image';
 
        //         // upload file
		// $file->move($tujuan_upload,$file->getClientOriginalName());
        
        
	}































    // public function viewBayar(){
    //     $auth = Auth::user()->id;
    //     $data_index= JenisTest::where([
    //         ['id_test', '=', $auth],
    //         ])->get();
    //     $data_daf= Daftar::where([
    //         ['id_daftar', '=', $auth],
    //         ])->get();
    //     //$data_index = tes::all();
    //     return view('pembayaran',compact('data_index','data_daf'));
    // }

    // public function tambahBayar(Request $request){
    //     $request->validate([
    //         'bukti'=>'required|image|max:2048',
    //     ]);
    //     $image_tes = $request->file('bukti');
    //     $gambar_baru = rand().'.'.$image_tes->getClientOriginalExtension();
    //     //biar gambar yg diunggah masuk ke file public-images, images itu folder baru untuk simpan data gambar otomatis terbuat
    //     $image_tes->move(public_path('images'),$gambar_baru); 
    //     //tambah data
    //     $insert_data = array(
    //         'id_daftar'=>$request->id_daftar,
    //         'bukti'=>$gambar_baru,
    //         'status'=>0, //waitinglist
    //     );
    //     Bayar::create($insert_data);
    //     return redirect('/pembayaran')->with('sukses','Pembayaran berhasil! Mohon menunggu konfirmasi dari Admin');
    // }

























    // public function viewBayar(){
    //     $auth = Auth::user()->id;
    //     $data_index= JenisTest::where([
    //         ['id_test', '=', $auth],
    //         ])->get();
    //     $data_daf= Daftar::where([
    //         ['id_daftar', '=', $auth],
    //         ])->get();
    //     //$data_index = tes::all();
    //     return view('pembayaran',compact('data_index','data_daf'));
    // }


    // public function viewjenistest()
    // {
    // 	$view = DB::table('jenistest_view')->get();
    // 	return view('status', ['view' => $view]);
    // }

    // public function tambahBayar(Request $request)
    // {

    //     $request->validate([
    //         'bukti' => 'mimes:jpeg,png,jpg,gif,svg',
    //         'nama' => 'required|min:10',
    //     ]);

    //     $imgName = $request->bukti->getClientOriginalExtension() . '-' . time() . '.' . $request->image->extension();
    //     $request->bukti->move(public_path('image'),$imgName);

    //     Bayar::create([
    //         'nama' => $request->nama,
    //         'bukti' => $imgName
    //     ]);
    //     return redirect('/pembayaran')->with('sukses','Pembayaran berhasil! Mohon menunggu konfirmasi dari Admin');

    // }

    // public function tambahBayar(Request $request){
    //     $request->validate([
    //         'nama_pengirim'=>'required',
    //         'jenis_bank'=>'required',
    //         'bukti'=>'required|image|max:2048',
    //     ]);
    //     $image_tes = $request->file('bukti');
    //     $gambar_baru = rand().'.'.$image_tes->getClientOriginalExtension();
    //     //biar gambar yg diunggah masuk ke file public-images, images itu folder baru untuk simpan data gambar otomatis terbuat
    //     $image_tes->move(public_path('images'),$gambar_baru); 
    //     //tambah data
    //     $insert_data = array(
    //         'id_daftar'=>$request->id_daftar,
    //         'nama_pengirim'=>$request->nama_pengirim,
    //         'jenis_bank'=>$request->jenis_bank,
    //         'status'=>0, //waitinglist
            
    //     );
    //     Bayar::create($insert_data);
    //     return redirect('/pembayaran/berhasil')->with('sukses','Pembayaran berhasil! Mohon menunggu konfirmasi dari Admin');
    // }
}

@extends('layouts.master')
@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Biodata</h1>
            </div>
            <div class="col-6">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Isi Biodata
                </button>
                
            </div>
                
                <table class = "table table-striped">
                    <tr>
                        <th>Nama</th>
                        <th>jenis kelamin</th>
                        <th>alamat</th>
                        <th>agama</th>
                        <th>email</th>
                        <th>no hp</th>
                        <th>aksi</th>
                    </tr>
                    @foreach($data_peserta as $peserta)
                    <tr>
                        <td>{{$peserta-> nama}}</td>
                        <td>{{$peserta-> jenis_kelamin}}</td>
                        <td>{{$peserta-> alamat}}</td>
                        <td>{{$peserta-> agama}}</td>
                        <td>{{$peserta-> email}}</td>
                        <td>{{$peserta-> no_hp}}</td>
                        <td>
                            <a href="/peserta/{{$peserta->id_peserta}}/edit" class= "btn btn-success btn-sm">Test</a>
                            <!-- <a href="/peserta/{{$peserta->id}}/edittest" class= "btn btn-success btn-sm">Test</a> -->
                            <a href="/peserta/{{$peserta->id_peserta}}/delete" class= "btn btn-danger btn-sm" onclick="return confirm('Yakin Hapus data ini?')">Hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
        </div>
    </div>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Peserta</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action = "/peserta/create" method="POST">
                        {{csrf_field()}}
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nama</label>
                                <input name="nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama">
                            </div>
                            <div class="mb-3">
                            <select name="jenis_kelamin" class="form-select" aria-label="Default select example">
                                <option selected>Pilih Jenis Kelamin </option>
                                <option value="Laki Laki">Laki Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                            </div>
                            <div class="mb-3">
                                <div class="form-floating">
                                    <textarea name="alamat" class="form-control" placeholder="Alamat" id="floatingTextarea2" style="height: 100px"></textarea>
                                    <label for="floatingTextarea2">Alamat</label>
                                </div>
                            </div>
                            <div class="mb-3">
                            <select name="agama" class="form-select" aria-label="Default select example">
                                <option selected>Agama </option>
                                <option value="Kristen">Kristen</option>
                                <option value="Islam">Islam</option>
                                <option value="Buddha">Buddha</option>
                                <option value="hindu">hindu</option>
                                <option value="Konghucu">Konghucu</option>
                                <option value="Dll">Dll</option>
                            </select>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Email</label>
                                <input name="email" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">No Hp</label>
                                <input name="no_hp"type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="No Hp">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    </div>
                </div>
    </div>
@endsection



      

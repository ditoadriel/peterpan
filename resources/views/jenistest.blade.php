@extends('layouts.admin')
@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Jenis Test</h1>
            </div>
            <div class="col-6">
                <!-- Button trigger modal -->
                <a href="/jenistestisi" class= "btn btn-success btn-sm">Jenis test</a>
                
            </div>
                
                <table class = "table table-striped">
                    <tr>
                        <th>No</th>
                        <th>Nama Test</th>
                        <th>Harga</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                    @foreach($data_jenistest as $test)
                    <tr>
                        <td>{{$loop-> iteration}}</td>
                        <td>{{$test-> nama_test}}</td>
                        <td>{{$test-> biaya}}</td>
                        <td>{{$test-> keterangan}}</td>
                        <td>
                        <a href="/jenistest/{{$test->id_test}}/delete" class= "btn btn-danger btn-sm" onclick="return confirm('Yakin Hapus data ini?')">Hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
        </div>
    </div>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Test</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action = "/jenistest/create" method="POST">
                        {{csrf_field()}}
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nama</label>
                                <input name="nama_test" type="text" class="form-control" id="nama_test" aria-describedby="emailHelp" placeholder="Nama Test">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Harga</label>
                                <input name="biaya" type="text" class="form-control" id="nama_test" aria-describedby="emailHelp" placeholder="Harga">
                            </div>
                            <div class="mb-3">
                            <label for="exampleInputEmail1">Keterangan</label>
                                <div class="form-floating">
                                    <textarea name="keterangan" class="form-control" placeholder="Keterangan" id="floatingTextarea2" style="height: 100px"></textarea>
                                    <label for="floatingTextarea2">Keterangan</label>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    </div>
                </div>
@endsection



      

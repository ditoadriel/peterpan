<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PesertaController extends Controller
{
    public function index()
    {
        $data_peserta = \App\Peserta::all();
        return view('peserta.index',['data_peserta' => $data_peserta]);
    }


    public function create(Request $request)
    {
        \App\Peserta::create($request->all());
        return redirect('/peserta')->with('sukses','Data Berhasil Di input');
    }

    public function edit($id_peserta)
    {
        $peserta = \App\Peserta::find($id_peserta);
        return view('peserta.edit',['peserta' => $peserta]);
    }

    public function update(Request $request,$id_peserta)
    {
        $peserta = \App\Peserta::find($id_peserta);
        $peserta->update($request->all());
        return redirect ('/peserta')->with('sukses','Data Berhasil Di update');
    }

    public function delete($id_peserta)
    {
        $peserta = \App\Peserta::find($id_peserta);
        $peserta->delete();
        return redirect('/peserta')->with('sukses','Data Berhasil Di hapus');
    }
}

@extends('layouts.master')

@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Status Pembayaran</h1>
            </div>
                <table class = "table table-striped">
                    <tr>
                        <th></th>
                        <th>No</th>
                        <th>nama </th>
                        <th>email</th>
                        <th>alamat</th>
                        <th></th>
                    </tr>
                    
                    @foreach($data_daf as $item)
                    <tr>
        
                        <td>{{$loop-> iteration}}</td>
                        <td>{{$item -> id}}</td>
                        <td>{{$item -> name}}</td>
                        <td>{{$item -> email}}</td>
                        <td>{{$item -> alamat}}</td>
                        
                        <td><a href="/alamat/{{$item -> id}}" type="button" class="btn btn-primary">Masukan Alamat</a></td>
            
                    
                    </tr>
                    @endforeach
                </table>
        </div>
    </div>    
    </div>    
@endsection



      

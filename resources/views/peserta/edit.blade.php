@extends('layouts.app')
@section('content')
    <h1>Biodata</h1>
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
        <div class="row">
        <form action = "/peserta/{{$peserta->id}}/update" method="POST">
                        {{csrf_field()}}
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nama</label>
                                <input name="nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama" value="{{$peserta->nama}}">
                            </div>
                            <div class="mb-3">
                            <select name="jenis_kelamin" class="form-select" aria-label="Default select example">
                                <option selected>Pilih </option>
                                <option value="Laki Laki" @if($peserta->jenis_kelamin == 'Laki Laki')selected @endif>Laki Laki</option>
                                <option value="Perempuan" @if($peserta->jenis_kelamin == 'Perempuan')selected @endif>Perempuan</option>
                            </select>
                            </div>
                            <div class="mb-3">
                                <div class="form-floating">
                                    <textarea name="alamat" class="form-control" placeholder="Alamat" id="floatingTextarea2" style="height: 100px">{{$peserta->alamat}}</textarea>
                                    <label for="floatingTextarea2">Alamat</label>
                                </div>
                            </div>
                            <div class="mb-3">
                            <select name="agama" class="form-select" aria-label="Default select example">
                                <option selected>Agama </option>
                                <option value="Kristen"@if($peserta->agama == 'Kristen')selected @endif>Kristen</option>
                                <option value="Islam"@if($peserta->agama == 'Islam')selected @endif>Islam</option>
                                <option value="Buddha"@if($peserta->agama == 'Buddha')selected @endif>Buddha</option>
                                <option value="hindu"@if($peserta->agama == 'hindu')selected @endif>hindu</option>
                                <option value="Konghucu"@if($peserta->agama == 'Konghucu')selected @endif>Konghucu</option>
                                <option value="Dll"@if($peserta->agama == 'Dll')selected @endif>Dll</option>
                            </select>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label" >Email</label>
                                <input name="email" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" value="{{$peserta->email}}">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">No Hp</label>
                                <input name="no_hp"type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="No Hp" value="{{$peserta->no_hp}}">
                            </div>  
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Edit</button>
        </form>
                    </div>
                    </div>
        </div>
    </div>
@endsection

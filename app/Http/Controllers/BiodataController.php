<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BiodataController extends Controller
{
    public function index()
    {
        $data_peserta = \App\Peserta::all();
        return view('biodata.index',['data_peserta' => $data_peserta]);
    }

    public function create(Request $request)
    {
        \App\Peserta::create($request->all());
        return redirect('/peserta')->with('sukses','Data Berhasil Di input');
    }

    public function edit($id)
    {
        $peserta = \App\Peserta::find($id);
        return view('peserta.edit',['peserta' => $peserta]);
    }

    public function update(Request $request,$id)
    {
        $peserta = \App\Peserta::find($id);
        $peserta->update($request->all());
        return redirect ('/peserta')->with('sukses','Data Berhasil Di update');
    }

    public function delete($id)
    {
        $peserta = \App\Peserta::find($id);
        $peserta->delete();
        return redirect('/peserta')->with('sukses','Data Berhasil Di hapus');
    }
}

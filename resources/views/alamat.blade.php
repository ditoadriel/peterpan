@extends('layouts.master')

@section('content')
@if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                    
                        
                        @foreach($data_daf as $df)
                        
                        <form method="POST" action="/alamat/update/{{$df->id}}">
                        @csrf
                        @method('PUT')
                            <input type="text" class="form-control" name="id" value='{{$df->id}}'>
                            <input type="text" class="form-control" name="alamat" value='{{$df->alamat}}'>
                        @endforeach
                    
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('UPDATE') }}
                                </button>
                            </div>
                        </div>
                        </form>
                
            
        </div>
    </div>
</div>
@endsection
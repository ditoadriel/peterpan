<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Daftar;
use App\JenisTest;
use App\Bayar;
use Auth;
use DB;

class VerifikasiController extends Controller
{
    public function index()
    {
        $data_daftar = Daftar::where('status',0)->get();
        return view('verifikasijadwal',compact('data_daftar'));
    }

    public function verifikasi(Request $req){
        Daftar::where('id_daftar', $req->get('id_daftar'))->update(['status' => $req->get('status')]);
        return redirect('/verifikasijadwal');
    }

    public function tampilPembayaran()
    {
        $data_daftar = Daftar::all();
        // return dd($data_daftar);
        return view('verifikasipembayaran',compact('data_daftar'));
    }

    public function verifikasipembayaran(Request $req)
    {
        Daftar::where('id_daftar', $req->get('id_daftar'))->update(['status_pembayaran' => $req->get('status_pembayaran')]);
        return redirect('/verifikasipembayaran');
    }

}

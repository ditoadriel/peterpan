<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bayar extends Model
{
    protected $table = 'pembayaran';
    protected $primaryKey = 'id_pembayaran';
    protected $fillable = ['id_daftar','id','bukti'];

    public function daftar(){
        return $this->belongsTo('App\Daftar');
    }
}

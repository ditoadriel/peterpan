<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Daftar extends Model
{
    protected $table = 'daftar';
    protected $primaryKey = 'id_daftar';
    protected $fillable = ['id_peserta','id_jadwal','status','bukti','status_pembayaran'];

}


@extends('layouts.master')
@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Daftar Test</h1>
            </div>
            <div class="col-6">
                <!-- Button trigger modal -->
                
            </div>
            @foreach($data_test as $test)
            <div class="card">
            <div class="card-header">
            </div>
            <div class="card">
            <div class="card-body">
                <h5 class="card-title"><h1>{{$test->nama_test}}</h1></h5>
                <p class="card-text">{{$test->keterangan}}</p>
                <p class="card-text">Biaya Test: {{$test->biaya}}</p>
                <a href="/daftar/{{$test->id_test}}" class="btn btn-primary">Pilih</a>
            </div>
            </div>
            <br>
            @endforeach
        </div>
    </div> 
     
    </div>
@endsection
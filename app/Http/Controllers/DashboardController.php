<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\jenisTest;
use App\Daftar;
use App\Jadwal;
use App\User;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $data_test = JenisTest::all();
        return view('dashboard',compact('data_test'));
    }

    // public function edit($id_peserta)
    // {
    //     $peserta = \App\Peserta::find($id_peserta);
    //     return view('peserta.edit',['peserta' => $peserta]);
    // }


    public function biodata(){
		$auth = Auth::user()->id;
        $data_daf= User::where([
            ['id', '=', $auth],
            ])->get();

        // return $data_daf;
        //$data_index = tes::all();
        return view('biodata',compact('data_daf'));
	}
    public function dfdf($id_daftar){
		$auth = Auth::user()->id;
        $data_daf= Daftar::where([
            ['id', '=', $auth],
            ])->value('id_daftar');
        //$data_index = tes::all();
        return view('pembayaran',compact('data_daf','id_daftar'));
	}

    public function upload($id, Request $request)
    {
        $user = \App\User::find($id);
            $user->alamat = $request->get('alamat');
            $user->save();
        return redirect ('/biodata')->with('sukses','Data Berhasil Di update');
    }

    public function ambil($id){
		$auth = Auth::user()->id;
        $data_daf= User::where([
            ['id', '=', $auth],
            ])->select('id','alamat')->get();

        //return $data_daf;
        return view('alamat',compact('data_daf'));
	}

    public function update(Request $request)
    {
        $user = \App\User::all();
        $user->update($request->all());

        $request->validate([
            'id' => 'id',
            'bukti.*' => 'mimes:doc,docx,PDF,pdf,jpg,jpeg,png|max:2000'
        ]);
        
        return redirect ('/biodata')->with('sukses','Data Berhasil Di update');
    }

    public function dsfdf(Request $request){

        $request->validate([
            'bukti' => 'required',
            'bukti.*' => 'mimes:doc,docx,PDF,pdf,jpg,jpeg,png|max:2000'
        ]);
        if ($request->hasfile('bukti')) {            
            $filename = round(microtime(true) * 1000).'-'.str_replace(' ','-',$request->file('bukti')->getClientOriginalName());
            $request->file('bukti')->storeAs('public/images',$filename);
             Daftar::where('id_daftar',$request->get('id_daftar'))->update([
                 'bukti' => $filename
             ]);
            return redirect('/statuspembayaran')->with('sukses','Pembayaran berhasil! Mohon menunggu konfirmasi dari Admin');
        }
    }


    public function view(){
        $daftar = Daftar::all();
        $jadwal = Jadwal::all();
        $data_test = JenisTest::all();

        $hasil = [];
        $data = [];
        foreach($data_test as $dt){
            $hasil[] = $dt->nama_test;
        }


        // dd(json_encode($data));


        foreach($data_test as $dt){
            $kategori = $dt->nama_test;
        }

        $data_daftar = User::where('role',0)->get();

        // return $hasil;
    return view('dashboardadmin',compact('data_daftar','data_test','hasil','data'));
    }
}

@extends('layouts.admin')
@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title"><h1>Data Pendaftar Sistem</h1></strong>
                        </div>
                        <div class="card-body">
                            <table class="table">
                              <thead>
                                <tr>
                                  <th scope="col">No</th>
                                  <th scope="col">Nama</th>
                                  <th scope="col">Email</th>
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($data_daftar as $item)
                            <tr>
                              <th scope="row">{{$loop-> iteration}}</th>
                              <td>{{$item -> name}}</td>
                              <td>{{$item -> email}}</td>
                            </tr>
                            @endforeach
                      </tbody>
                  </table>
                        </div>
                        
                    </div>
                    
                    
                    
                </div>
                <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">   </div>

                <div class="card-body">
                    <div id = "chart">
                        <script>
                            Highcharts.chart('chart', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'DATA JENIS TEST'
                            },
                            subtitle: {
                                text: 'Source: RAHASIA WAL'
                            },
                            xAxis: {
                                categories: {!!json_encode($hasil)!!},
                                crosshair: true
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Rainfall (mm)'
                                }
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                            series: [{
                                name: 'Data Pengambil',
                                data: [2,3,1,5,6,1]

                            }]
                        });
               </script>
            </div>
            
        </div>
    </div>
    
    
@endsection

@section('footer')
@endsection
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisTest;
use App\Jadwal;

class JadwalController extends Controller
{
    public function index()
    {
        $data_jadwal = Jadwal::join('jenis_test','jenis_test.id_test','=','jadwal.id_test')
                                ->get(['jenis_test.nama_test','jadwal.jadwal_test','jadwal.jam_mulai','jadwal.kapasitas','jadwal.link_pertemuan','jadwal.id_jadwal']);
        $data_test = JenisTest::all();
        return view('jadwal',compact('data_jadwal','data_test'));
    }

    public function form()
    {
        $data_jadwal = Jadwal::join('jenis_test','jenis_test.id_test','=','jadwal.id_test')
                                ->get(['jenis_test.nama_test','jadwal.jadwal_test','jadwal.jam_mulai','jadwal.kapasitas','jadwal.link_pertemuan','jadwal.id_jadwal']);
        $data_test = JenisTest::all();
        return view('jadwalisi',compact('data_jadwal','data_test'));
    }


    public function create(Request $request)
    {
        \App\Jadwal::create($request->all());
        return redirect('/jadwal')->with('sukses','Data Berhasil Di input');
    }

    public function delete($id_jadwal)
    {
        
        $jadwal_delete = \App\Jadwal::find($id_jadwal);
        $jadwal_delete->delete();
        return redirect('/jadwal')->with('sukses','Data Berhasil Di hapus');
    }
}

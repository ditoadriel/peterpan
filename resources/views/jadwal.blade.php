@extends('layouts.admin')
@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Jadwal Test</h1>
            </div>
            <div class="col-6">
                <!-- Button trigger modal -->
                <a href="/jadwalisi" class= "btn btn-success btn-sm">Tambah Jadwal</a>
                
            </div>
                
                <table class = "table table-striped">
                    <tr>
                        <th>No</th>
                        <th>Nama Test</th>
                        <th>Jadwal Test</th>
                        <th>Pelaksanaan</th>
                        <th>Kapasitas</th>
                        <th>Link Pertemuan</th>
                        <th>Aksi</th>
                    </tr>
                    
                    @foreach($data_jadwal as $jadwal)
                    <tr>
                        <td>{{$loop-> iteration}}</td>
                        <td>{{$jadwal-> nama_test}}</td>
                        <td>{{$jadwal-> jadwal_test}}</td>
                        <td>{{$jadwal-> jam_mulai}}</td>
                        <td>{{$jadwal-> kapasitas}}</td>
                        <td>{{$jadwal-> link_pertemuan}}</td>
                        <td>
                        <a href="/jadwal/{{$jadwal->id_jadwal}}/delete" class= "btn btn-danger btn-sm" onclick="return confirm('Yakin Hapus data ini?')">Hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
        </div>
    </div>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Jadwal</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action = "/jadwal/create" method="POST">
                        {{csrf_field()}}
                            <label for="exampleInputEmail1">Pilih Test </label>
                            <select name="id_test" id="id_test" class="form control input-sm">
                            @foreach($data_test as $test)
                                <option value="{{$test->id_test}}">{{$test->nama_test}}</option>
                            @endforeach
                            </select>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Tanggal</label>
                                <input name="jadwal_test" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tanggal">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Jam Mulai</label>
                                <input name="jam_mulai" type="time" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tanggal">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Kapasitas</label>
                                <input name="kapasitas" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Kapasitas">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Link Pertemuan</label>
                                <input name="link_pertemuan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Link Pertemuan">
                            </div>
                            
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    </div>
                </div>
@endsection



      

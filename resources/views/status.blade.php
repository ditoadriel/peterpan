@extends('layouts.master')

@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Status</h1>
            </div>
                <table class = "table table-striped">
                    <tr>
                        <th>No</th>
                        <th>ID Daftar</th>
                        <th>Nama</th>
                        <th>Tes</th>
                        <th>Jadwal</th>
                        <th>Harga</th>
                        <th>Status</th>
                        <!-- <th>Transaksi</th>
                        <th>Bukti</th>
                        <th>Status Pembayaran</th>
                        <th>Jam Pelaksanaan</th>
                        <th>Link Pertemuan</th> -->
                    </tr>
                    
                    @foreach($data_view as $item)
                    <tr>
                        <td>{{$loop-> iteration}}</td>
                        <td>{{$item -> id_daftar}}</td>
                        <td>{{DB::table('users')->where('id', $item['id'])->value('name')}}</td>
                        <td>{{DB::table('jenis_test')->where('id_test',DB::table('jadwal')->where('id_jadwal', $item['id_jadwal'])->value('id_test'))->value('nama_test')}}</td>
                        <td>{{DB::table('jadwal')->where('id_jadwal', $item['id_jadwal'])->value('jadwal_test')}}</td>
                        <td>{{DB::table('jenis_test')->where('id_test',DB::table('jadwal')->where('id_jadwal', $item['id_jadwal'])->value('id_test'))->value('biaya')}}</td>
                        
                        @if($item->status == 0)
                                        <td>Menunggu</td>
                                        <!-- <td>
                                            <button type="button" class="btn btn-danger">
                                            Bayar
                                            </button>   
                                        </td> -->
                                        @elseif($item->status == 1)
                                        <td>Diterima</td>
                                        <!-- <td>
                                            <a href="/pembayaran/{{$item -> id_daftar}}" type="button" class="btn btn-primary">Bayar</a>
                                            <td><a href="storage/images/{{$item->bukti}}">Tekan Disini</a></td>
                                        </td> -->
                                                <!-- @if($item->status_pembayaran == 0)
                                                <td>Menunggu</td>
                                                @elseif($item->status_pembayaran == 1)
                                                <td>Diterima</td>
                                                <td>{{DB::table('jadwal')->where('id_jadwal', $item['id_jadwal'])->value('jam_mulai')}}</td>
                                                <td>{{DB::table('jadwal')->where('id_jadwal', $item['id_jadwal'])->value('link_pertemuan')}}</td>
                                                @else
                                                <td>Ditolak</td>
                                                @endif -->
                                        @else
                                        <td>Ditolak</td>
                        @endif
                    </tr>
                    @endforeach
                </table>
        </div>
    </div>    
    </div>    
@endsection



      

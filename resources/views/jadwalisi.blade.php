@extends('layouts.admin')
@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <h1>Input jenis test</h1>
    <div class="modal-body">
                        <form action = "/jadwal/create" method="POST">
                        {{csrf_field()}}
                            <label for="exampleInputEmail1">Pilih Test </label>
                            <select name="id_test" id="id_test" class="form control input-sm">
                            @foreach($data_test as $test)
                                <option value="{{$test->id_test}}">{{$test->nama_test}}</option>
                            @endforeach
                            </select>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Tanggal</label>
                                <input name="jadwal_test" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tanggal">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Jam Mulai</label>
                                <input name="jam_mulai" type="time" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tanggal">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Kapasitas</label>
                                <input name="kapasitas" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Kapasitas">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Link Pertemuan</label>
                                <input name="link_pertemuan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Link Pertemuan">
                            </div>
                            
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
    
    
@endsection
@section('footer')
@endsection
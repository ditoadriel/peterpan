<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisTest;
use App\Daftar;
use App\Bayar;
use App\DB;
use Auth;

class StatusController extends Controller
{

    // public function viewPilih(){
    //     $auth = Auth::user()->id;
    //     $data_view= Daftar::where([
    //         ['id', '=', $auth],
    //         ])->get();
    //     //$data_view = pendaftaran::all();
    //     //$data_view = DB::table('jadwal')->join('pendaftaran','jadwal.id_jadwal','=','pendaftaran.id_daftar')->get();
    //     return view('status',compact('data_view'));
    // }

    public function index(){
        $auth = Auth::user()->id;
        $data_view= Daftar::where([
            ['id', '=', $auth],
            ])->get();
        $verTrx = Bayar::all();
        //$data_view = pendaftaran::all();
        //$data_view = DB::table('jadwal')->join('pendaftaran','jadwal.id_jadwal','=','pendaftaran.id_daftar')->get();
        return view('status',compact('data_view','verTrx'));
    }

    public function bayar(){
        $auth = Auth::user()->id;
        $data_view= Daftar::where([
            ['id', '=', $auth],
            ])->get();
        $verTrx = Bayar::all();
        //$data_view = pendaftaran::all();
        //$data_view = DB::table('jadwal')->join('pendaftaran','jadwal.id_jadwal','=','pendaftaran.id_daftar')->get();
        return view('statuspembayaran',compact('data_view','verTrx'));
    }

    // public function index()
    // {   
    //     $auth = Auth::user()->id;
    //     $data_view= Daftar::where([
    //         ['id', '=', $auth],
    //         ])->get();
    //     // $data_daf= Daftar::where([
    //     //     ['id_daftar', '=', $auth],
    //     //     ])->get();
    //     $data_index= JenisTest::where([
    //             ['id_test', '=', $auth],
    //             ])->get();
    //     $data_daf= Daftar::where([
    //         ['id_daftar', '=', $auth],
    //         ])->get();
    //     return view('status',compact('data_view','data_index','data_daf'));

    // }

    // public function viewBayar(){
    //     $data_bayar = DB::table('jadwal')->join('daftar','jadwal.id_jadwal','=','daftar.id_daftar')->get();
    //     return view('status',compact('data_bayar'));
    // }

    // public function viewBayar(){
    //     $auth = Auth::user()->id;
    //     $data_index= JenisTest::where([
    //         ['id_test', '=', $auth],
    //         ])->get();
    //     $data_daf= Daftar::where([
    //         ['id_daftar', '=', $auth],
    //         ])->get();
    //     //$data_index = tes::all();
    //     return view('pembayaran',compact('data_index','data_daf'));
    // }


    // public function viewjenistest()
    // {
    // 	$view = DB::table('jenistest_view')->get();
    // 	return view('status', ['view' => $view]);
    // }

    // public function tambahBayar(Request $request){
    //     $request->validate([
    //         'nama_pengirim'=>'required',
    //         'jenis_bank'=>'required',
    //         'bukti'=>'required|image|max:2048',
    //     ]);
    //     $image_tes = $request->file('bukti');
    //     $gambar_baru = rand().'.'.$image_tes->getClientOriginalExtension();
    //     //biar gambar yg diunggah masuk ke file public-images, images itu folder baru untuk simpan data gambar otomatis terbuat
    //     $image_tes->move(public_path('images'),$gambar_baru); 
    //     //tambah data
    //     $insert_data = array(
    //         'nama_pengirim'=>$request->nama_pengirim,
    //         'jenis_bank'=>$request->jenis_bank,
    //         'bukti_bayar'=>$gambar_baru,
    //         'status'=>0, //waitinglist
    //         'id_daftar'=>$request->id_daftar
    //     );
    //     transaksi::create($insert_data);
    //     return redirect('/pembayaran/berhasil')->with('sukses','Pembayaran berhasil! Mohon menunggu konfirmasi dari Admin');
    // }
    
}

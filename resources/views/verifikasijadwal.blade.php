@extends('layouts.admin')

@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Verifikasi Jadwal</h1>
            </div>
                <table class = "table table-striped">
                    <tr>
                        <th>No</th>
                        <th>ID Daftar</th>
                        <th>Nama</th>
                        <th>Tes</th>
                        <th>Jadwal</th>
                        <th>Status</th>
                        <th>Aksi</th>
                        <th></th>
                    </tr>
                    
                    @foreach($data_daftar as $item)
                    <tr>
                        <td>{{$loop-> iteration}}</td>
                        <td>{{$item-> id_daftar}}</td>
                        <td>{{DB::table('users')->where('id', $item['id'])->value('name')}}</td>
                        <td>{{DB::table('jenis_test')->where('id_test',DB::table('jadwal')->where('id_jadwal', $item['id_jadwal'])->value('id_test'))->value('nama_test')}}</td>
                        <td>{{DB::table('jadwal')->where('id_jadwal', $item['id_jadwal'])->value('jadwal_test')}}</td>
                                    @if($item->status == 0)

                                        <td>Menunggu</td>
                                        <td><form method="post" action="/verifikasijadwal/pilih">
                                    @csrf
                                                <input type="hidden" name="id_daftar" value="{{$item->id_daftar}}">
                                                <input type="hidden" name="status" value="1">
                                                <button type="submit" class="btn btn-success"> Terima </button>

                                                </form>
                                    </td>
                                    <td><form method="post" action="/verifikasijadwal/pilih">
                                    @csrf
                                                <input type="hidden" name="status" value="2">
                                                <input type="hidden" name="id_daftar" value="{{$item->id_daftar}}">
                                                <button type="submit" class="btn btn-danger"> Tolak </button>
                                                
                                                </form>
                                    </td>
                                        @elseif($item->status == 1)
                                        <td>Diterima</td>
                                        <td><form method="post" action="/verifikasijadwal/pilih">
                                    @csrf
                                                <input type="hidden" name="status" value="2">
                                                <input type="hidden" name="id_daftar" value="{{$item->id_daftar}}">
                                                <button type="submit" class="btn btn-danger"> Tolak </button>
                                                
                                                </form>
                                    </td>
                                        @else
                                        <td>Ditolak</td>
                                        <td><form method="post" action="/verifikasijadwal/pilih">
                                    @csrf
                                                <input type="hidden" name="id_daftar" value="{{$item->id_daftar}}">
                                                <input type="hidden" name="status" value="1">
                                                <button type="submit" class="btn btn-success"> Terima </button>

                                                </form>
                                    </td>
                                    @endif
                                    
                    </tr>
                    @endforeach
                </table>
        </div>
    </div>    
    </div>       
@endsection



      

@extends('layouts.master')

@section('content')
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Daftar Test</h1>
            </div>
                <table class = "table table-striped">
                    <tr>
                        <th>No</th>
                        <th>Nama Test</th>
                        <th>Jadwal</th>
                        <th>Jam Mulai</th>
                        <th>Kapasitas</th>
                        <th>Link Pertemuan</th>
                        <th>Aksi</th>
                    </tr>
                    
                    @foreach($data as $item)
                    <tr>
                        <td>{{$loop-> iteration}}</td>
                        <td>{{DB::table('jenis_test')->where('id_test',DB::table('jadwal')->where('id_jadwal', $item['id_jadwal'])->value('id_test'))->value('nama_test')}}</td>
                        <td>{{$item-> jadwal_test}}</td>
                        <td>{{$item-> jam_mulai}}</td>
                        
                        <td>{{$item-> kapasitas}}</td>
                        <td>{{$item-> link_pertemuan}}</td>
                        <form action = "/daftar/pilih/{{$item-> id_jadwal}}" method="POST">
                        @csrf
                            <input type="hidden" name="id_daftar" value="{{$item['id_daftar']}}">
                            <input type="hidden" name="id_jadwal" value="{{$item['id_jadwal']}}">
                            <td>
                                <button type="submit" class="btn btn-primary">PILIH</button>
                            </td>
                        </form>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>    
    </div> 

@endsection



      

<?php
use App\Http\Controllers\UserController;
use App\Http\Middleware\CheckLogin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/mhs', 'MahasiswaController@mhs')->middleware(CheckLogin::class);


Route::get('/', function () {
    return view('welcome');
});

Route::get('/main', function () {
    return view('main');
});
Route::get('/jenistestisi', function () {
    return view('jenistestisi');
});
// Route::get('/jadwalisi', function () {
//     return view('jadwalisi');
// });



Route::get('/login', function () {
    return view('login');
});

Route::get('/pembayaran', function () {
    return view('pembayaran');
});

Route::get('/peserta','PesertaController@index');
Route::post('/peserta/create','PesertaController@create');
Route::get('/peserta/{id_peserta}/edit','PesertaController@edit');
Route::post('/peserta/{id_peserta}/update','PesertaController@update');
Route::get('/peserta/{id_peserta}/delete','PesertaController@delete');
Route::post('/peserta/{id_peserta}/test','PesertaController@test');



Route::get('/jenistest','JenisTestController@index');
Route::post('/jenistest/create','JenisTestController@create');
Route::get('/jenistest/{id_test}/delete','JenisTestController@delete');



Route::get('/jadwal','jadwalController@index');
Route::get('/jadwalisi','jadwalController@form');
Route::post('/jadwal/create','jadwalController@create');
Route::get('/jadwal/{id_jadwal}/delete','jadwalController@delete');



Route::get('/daftar','DaftarController@index');
Route::post('/daftar/create','DaftarController@create');
Route::get('/daftar/{id_daftar}/edit','DaftarController@edit');
Route::post('/daftar/{id_daftar}/update','DaftarController@update');
Route::get('/daftar/{id_daftar}/delete','DaftarController@delete');
Route::get('/daftar/{id_test}','DaftarController@viewDaftar');


Route::get('/verifikasijadwal','VerifikasiController@index');
Route::get('/verifikasipembayaran','VerifikasiController@tampilPembayaran');
Route::post('/verifikasipembayaran/pilih','VerifikasiController@verifikasipembayaran');
Route::post('/verifikasijadwal/pilih','VerifikasiController@verifikasi');

Route::get('/status','StatusController@index');
Route::get('/statuspembayaran','StatusController@bayar');


Route::get('/pembayaran/{id_daftar}','PembayaranController@upload');
Route::post('/pembayaran/upload','PembayaranController@proses_upload');





Route::post('/daftar/pilih/{id_jadwal}','DaftarController@pilihJadwal');


Route::get('/dashboardadmin','DashboardController@view');
Route::get('/dashboard','DashboardController@index');


Route::get('/biodata','DashboardController@biodata');
Route::get('/alamat/{id}','DashboardController@ambil');
Route::put('/alamat/update/{id}','DashboardController@upload');








Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

@extends('layouts.app')
@section('content')
    <h1>Edit Data Peserta</h1>
    @if(session('sukses'))
    <div class="alert alert-success" role="alert">
    {{session('sukses')}}
    </div>
    @endif
    <div class="container">
        <div class="row">
        <form action = "/peserta/{{$peserta->id}}/update" method="POST">
                        {{csrf_field()}}
                            <div class="mb-3">
                            <select name="agama" class="form-select" aria-label="Default select example">
                                <option selected>Agama </option>
                                <option value="Kristen"@if($peserta->agama == 'Kristen')selected @endif>Kristen</option>
                                <option value="Islam"@if($peserta->agama == 'Islam')selected @endif>Islam</option>
                                <option value="Buddha"@if($peserta->agama == 'Buddha')selected @endif>Buddha</option>
                                <option value="hindu"@if($peserta->agama == 'hindu')selected @endif>hindu</option>
                                <option value="Konghucu"@if($peserta->agama == 'Konghucu')selected @endif>Konghucu</option>
                                <option value="Dll"@if($peserta->agama == 'Dll')selected @endif>Dll</option>
                            </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">UPDATE</button>
        </form>
                    </div>
                    </div>
        </div>
    </div>
@endsection
